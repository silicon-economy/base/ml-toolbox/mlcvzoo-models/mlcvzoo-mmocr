# MLCVZoo MMOCR

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_mmocr** is the wrapper module for
the [mmocr framework](https://github.com/open-mmlab/mmocr).

## Install
`
pip install mlcvzoo-mmocr
`

# Further documentation
[comment]: <> (TODO: Fix links to main branch)
- [Ar42 Documentation](https://git.openlogisticsfoundation.org/silicon-economy/base/ml-toolbox/mlcvzoo-models/mlcvzoo-mmocr/-/blob/main/documentation/index.adoc)
- [Installation guide](https://git.openlogisticsfoundation.org/silicon-economy/base/ml-toolbox/mlcvzoo-models/mlcvzoo-mmocr/-/blob/main/documentation/12_tutorial.adoc?ref_type=heads#user-content-setup-the-mlcvzoo-models)
- [Configuration Usage](https://git.openlogisticsfoundation.org/silicon-economy/base/ml-toolbox/mlcvzoo-models/mlcvzoo-mmocr/-/blob/main/documentation/12_tutorial.adoc?ref_type=heads#user-content-setup-the-mlcvzoo-models)


## Technology stack

- Python
