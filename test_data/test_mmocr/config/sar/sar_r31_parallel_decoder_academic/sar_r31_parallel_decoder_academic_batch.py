_base_ = [
    "MMOCR_DIR/configs/textrecog/sar/sar_resnet31_parallel-decoder_5e_st-sub_mj-sub_sa_real.py"
]

# NOTE:
# Modify test-pipeline in order to be able to do batch-inference.
# The original configuration with 'MultiRotateAugOCR' does not support this
test_pipeline = [
    dict(type="LoadImageFromFile"),
    dict(
        type="ResizeOCR",
        height=48,
        min_width=48,
        max_width=160,
        keep_aspect_ratio=True,
        width_downsample_ratio=0.25,
    ),
    dict(type="ToTensorOCR"),
    dict(type="NormalizeOCR", mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5]),
    dict(
        type="Collect",
        keys=["img"],
        meta_keys=[
            "filename",
            "ori_shape",
            "resize_shape",
            "valid_ratio",
        ],
    ),
]

data = dict(
    val=dict(pipeline=test_pipeline),
    test=dict(pipeline=test_pipeline),
)
