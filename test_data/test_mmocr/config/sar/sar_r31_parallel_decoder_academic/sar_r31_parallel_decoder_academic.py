_base_ = [
    "MMOCR_DIR/configs/textrecog/sar/sar_resnet31_parallel-decoder_5e_st-sub_mj-sub_sa_real.py"
]

train_dataset = dict(
    type="RecogTextDataset",
    data_root="PROJECT_ROOT_DIR",
    ann_file="PROJECT_ROOT_DIR/test_data/annotations/text_recognition_test_label.txt",
    parser_cfg=dict(type="LineStrParser", separator=";"),
)
test_dataset = dict(
    type="RecogTextDataset",
    data_root="PROJECT_ROOT_DIR",
    ann_file="PROJECT_ROOT_DIR/test_data/annotations/text_recognition_test_label.txt",
)

test_dataloader = dict(
    batch_size=1,
    num_workers=4,
    persistent_workers=True,
    drop_last=False,
    sampler=dict(type="DefaultSampler", shuffle=False),
    dataset=dict(
        type="ConcatDataset", datasets=[test_dataset], pipeline=_base_.test_pipeline
    ),
)

train_dataloader = dict(
    batch_size=2,
    num_workers=1,
    persistent_workers=True,
    sampler=dict(type="DefaultSampler", shuffle=True),
    dataset=dict(
        type="ConcatDataset", datasets=[train_dataset], pipeline=_base_.train_pipeline
    ),
)

val_dataloader = None
val_cfg = None
val_evaluator = None
