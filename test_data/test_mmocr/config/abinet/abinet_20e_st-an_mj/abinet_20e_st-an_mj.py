_base_ = [
    "MMOCR_DIR/configs/textrecog/_base_/default_runtime.py",
    "MMOCR_DIR/configs/textrecog/_base_/schedules/schedule_adam_base.py",
    "MMOCR_DIR/configs/textrecog/abinet/_base_abinet.py",
]

optim_wrapper = dict(optimizer=dict(lr=1e-4))
train_cfg = dict(max_epochs=20)
# learning policy
param_scheduler = [
    dict(type="LinearLR", end=2, start_factor=0.001, convert_to_iter_based=True),
    dict(type="MultiStepLR", milestones=[16, 18], end=20),
]

train_dataset = dict(
    type="RecogTextDataset",
    pipeline=_base_.train_pipeline,
    parser_cfg=dict(type="LineStrParser", separator=" "),
)
train_dataloader = dict(
    batch_size=192 * 4,
    num_workers=32,
    persistent_workers=True,
    sampler=dict(type="DefaultSampler", shuffle=True),
    dataset=train_dataset,
)

test_dataset = dict(
    type="RecogTextDataset",
    pipeline=_base_.test_pipeline,
)

test_dataloader = dict(
    batch_size=1,
    num_workers=4,
    persistent_workers=True,
    drop_last=False,
    sampler=dict(type="DefaultSampler", shuffle=False),
    dataset=test_dataset,
)
test_evaluator = [
    dict(type='WordMetric', mode=['exact', 'ignore_case', 'ignore_case_symbol']),
    dict(type='CharMetric')
]

test_cfg = dict(type='TestLoop')

val_dataloader = test_dataloader
val_evaluator = test_evaluator
val_cfg = test_cfg

auto_scale_lr = dict(base_batch_size=192 * 8)
