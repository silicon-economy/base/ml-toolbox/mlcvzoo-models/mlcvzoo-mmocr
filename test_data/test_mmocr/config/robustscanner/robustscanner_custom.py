_base_ = [
    "MMOCR_DIR/configs/textrecog/_base_/default_runtime.py",
    "MMOCR_DIR/configs/textrecog/_base_/schedules/schedule_adam_step_5e.py",
    "MMOCR_DIR/configs/textrecog/robust_scanner/_base_robustscanner_resnet31.py",
]

default_hooks = dict(
    logger=dict(type="LoggerHook", interval=100),
    # mlflow_logger=dict(type="MLflowLoggerHook"),
)

# dataset settings
train_dataset = dict(
    type="RecogTextDataset",
    data_root="PROJECT_ROOT_DIR",
    ann_file="PROJECT_ROOT_DIR/test_data/annotations/text_recognition_test_label.txt",
    pipeline=_base_.train_pipeline,
    parser_cfg=dict(type="LineStrParser", separator=";"),
)
train_dataloader = dict(
    batch_size=64 * 4,
    num_workers=1,
    persistent_workers=True,
    sampler=dict(type="DefaultSampler", shuffle=True),
    dataset=train_dataset,
)

test_dataset = dict(
    type="RecogTextDataset",
    data_root="PROJECT_ROOT_DIR",
    ann_file="PROJECT_ROOT_DIR/test_data/annotations/text_recognition_test_label.txt",
    pipeline=_base_.test_pipeline,
    parser_cfg=dict(type="LineStrParser", separator=";"),
)
test_dataloader = dict(
    batch_size=1,
    num_workers=1,
    persistent_workers=True,
    drop_last=False,
    sampler=dict(type="DefaultSampler", shuffle=False),
    dataset=test_dataset,
)
test_evaluator = [
    dict(type="WordMetric", mode=["exact", "ignore_case", "ignore_case_symbol"]),
    dict(type="CharMetric"),
]
test_cfg = dict(type="TestLoop")
val_dataloader = test_dataloader
val_evaluator = test_evaluator
val_cfg = dict(type="ValLoop")
param_scheduler = [
    dict(end=5, milestones=[
        3,
        4,
    ], type='MultiStepLR'),
]
