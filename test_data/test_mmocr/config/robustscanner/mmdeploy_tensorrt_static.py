_base_ = [
    "MMDEPLOY_DIR/configs/mmocr/text-recognition/text-recognition_static.py",
    "MMDEPLOY_DIR/configs/_base_/backends/tensorrt.py",
]

onnx_config = dict(input_shape=(100, 29))

backend_config = dict(
    common_config=dict(max_workspace_size=1 << 30),
    model_inputs=[
        dict(
            input_shapes=dict(
                input=dict(
                    min_shape=[1, 3, 48, 160],
                    opt_shape=[1, 3, 48, 160],
                    max_shape=[1, 3, 48, 160],
                )
            )
        )
    ],
)
