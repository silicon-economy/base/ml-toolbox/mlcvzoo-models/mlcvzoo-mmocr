_base_ = [
    "MMDEPLOY_DIR/configs/mmocr/text-detection/text-detection_static.py",
    "MMDEPLOY_DIR/configs/_base_/backends/tensorrt.py",
]

# Images are reshaped to 1088x640
onnx_config = dict(input_shape=(1088, 640))

backend_config = dict(
    common_config=dict(max_workspace_size=1 << 30),
    model_inputs=[
        dict(
            input_shapes=dict(
                input=dict(
                    min_shape=[1, 3, 640, 1088],
                    opt_shape=[1, 3, 640, 1088],
                    max_shape=[1, 3, 640, 1088],
                )
            )
        )
    ],
)
