_base_ = ["MMOCR_DIR/configs/textdet/panet/panet_resnet18_fpem-ffm_600e_ctw1500.py"]

# NOTE:
# Modify test-pipeline in order to be able to do batch-inference.
# The original configuration with 'MultiRotateAugOCR' does not support this
test_pipeline = [
    dict(type="LoadImageFromFile", color_type="color_ignore_orientation"),
    dict(type="Resize", img_scale=(3000, 640), keep_ratio=True),
    dict(
        type="Normalize",
        mean=[123.675, 116.28, 103.53],
        std=[58.395, 57.12, 57.375],
        to_rgb=True,
    ),
    dict(type="Pad", size_divisor=32),
    dict(type="ImageToTensor", keys=["img"]),
    dict(
        type="Collect",
        keys=["img"],
        meta_keys=[
            "filename",
            "ori_filename",
            "ori_shape",
            "img_shape",
            "pad_shape",
            "scale_factor",
            "img_norm_cfg",
        ],
    ),
]

data = dict(
    val=dict(pipeline=test_pipeline),
    test=dict(pipeline=test_pipeline),
)
