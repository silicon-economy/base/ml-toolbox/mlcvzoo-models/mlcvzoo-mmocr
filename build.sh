#!/bin/sh

TARGET_PYTHON_VERSION=$(python3 -c 'import platform;print("".join(platform.python_version().split(".")[0:2]))')

REQUIREMENTS_FILE="${REQUIREMENTS_FILE:=./requirements_locked/requirements-lock-uv-py$TARGET_PYTHON_VERSION-all.txt}"

if [ -z $CUDA_VERSION_REDUCED ]; then
  CUDA_VERSION_REDUCED=$($CUDA_HOME/bin/nvcc --version | grep 'Cuda compilation tools' | awk '{print $5}' | tr -d . | tr -d ,)
fi

# Currently there are network issues with uv pip sync, therefore install via pip directly
# Sync dependencies with uv, this will remove all packages, that are not part of the .txt file
#uv pip sync "$REQUIREMENTS_FILE"
python3 -m pip install -r "$REQUIREMENTS_FILE"

# Reinstall and compile mmcv with legacy setup, since the pulled pypi package does not fit to the system environment
export TORCH_VERSION="$(pip show torch | grep Version | awk '{print $2}')"
export MMCV_VERSION="$(pip show mmcv | grep Version | awk '{print $2}')"

python -m pip uninstall -y mmcv

# Try to install a prebuilt version from openmmlab.
# A complete list of version compatibility between mmcv, torch and cuda can be found here:
# https://mmcv.readthedocs.io/en/latest/get_started/installation.html#install-with-pip
python -m pip install \
  --no-cache mmcv=="$MMCV_VERSION" \
  --no-index \
  --find-links "https://download.openmmlab.com/mmcv/dist/cu$CUDA_VERSION_REDUCED/torch$TORCH_VERSION/index.html"

if [ "$?" -eq 0 ]; then
  echo "Install of mmcv from mirror succeeded"
else
  python -m pip uninstall -y mmcv
  echo "Install of mmcv from mirror failed, compile mmcv package..."
  python -m pip install setuptools wheel
  # MMCV_WITH_OPS=1 is needed to build the full version with CPU
  # MMCV_WITH_OPS=0 would build the lite version of mmcv
  MMCV_WITH_OPS=1 python -m pip install \
    --no-cache \
    --no-binary :all: \
    --no-deps \
    --no-build-isolation \
    --no-use-pep517 \
    mmcv=="$MMCV_VERSION"
fi
