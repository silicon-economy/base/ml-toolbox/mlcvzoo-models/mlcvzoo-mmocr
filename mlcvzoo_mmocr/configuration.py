# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Definition of the MMOCRConfig that is used to configure the MMOCRModel (and subclasses).
"""

from __future__ import annotations

import logging
from typing import Optional

import related
from attr import Factory, define
from config_builder import BaseConfigClass
from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig
from mlcvzoo_base.configuration.class_mapping_config import (
    ClassMappingConfig,
    ClassMappingModelClassesConfig,
)
from mlcvzoo_mmdetection.configuration import (
    MMConfig,
    MMDetectionConfig,
    MMDetectionDistributedTrainConfig,
    MMDetectionInferenceConfig,
    MMDetectionMMDeployOnnxruntimeConfig,
    MMDetectionMMDeployTensorRTConfig,
    MMDetectionTrainArgparseConfig,
    MMDetectionTrainConfig,
)

logger = logging.getLogger(__name__)


@define
class MMOCRTrainDatasetConfig(BaseConfigClass):
    """Config for the datasets used during training"""

    data_root_dir: str = related.StringField(default="")

    padding_ratio: float = related.FloatField(default=0.1)

    train_annotation_handler_config: Optional[AnnotationHandlerConfig] = (
        related.ChildField(cls=AnnotationHandlerConfig, default=None)
    )

    val_annotation_handler_config: Optional[AnnotationHandlerConfig] = (
        related.ChildField(cls=AnnotationHandlerConfig, default=None)
    )

    test_annotation_handler_config: Optional[AnnotationHandlerConfig] = (
        related.ChildField(cls=AnnotationHandlerConfig, default=None)
    )


@define
class MMOCRTrainArgparseConfig(MMDetectionTrainArgparseConfig):
    """Config for the training arguments."""

    __related_strict__ = True
    # argparse parameter from mmdetection:

    # The checkpoint file to load from.
    load_from: Optional[str] = related.StringField(required=False, default=None)

    # Memory cache config for image loading speed-up during training.
    mc_config: Optional[str] = related.StringField(required=False, default=None)

    # NOTE: The following argparse arguments from mmdet.tools.train will not be used in this
    #       configuration.
    #
    # - local_rank: int = related.StringField(default=0) rank for distributed training

    def check_values(self) -> bool:
        if self.load_from is not None:
            logger.warning(
                "DEPRECATED: The load_from config attribute is no longer supported "
                "and will be removed in future versions"
            )

        if self.mc_config is not None:
            logger.warning(
                "DEPRECATED: The mc_config config attribute is no longer supported "
                "and will be removed in future versions"
            )

        return True


@define
class MMOCRTrainConfig(MMDetectionTrainConfig):
    """Config for the training process."""

    __related_strict__ = True

    argparse_config: MMOCRTrainArgparseConfig = related.ChildField(
        cls=MMOCRTrainArgparseConfig
    )

    best_metric_name: Optional[str] = related.StringField(required=False, default=None)

    multi_gpu_config: Optional[MMDetectionDistributedTrainConfig] = related.ChildField(
        cls=MMDetectionDistributedTrainConfig, required=False, default=None
    )

    dataset_config: Optional[MMOCRTrainDatasetConfig] = related.ChildField(
        cls=MMOCRTrainDatasetConfig, required=False, default=None
    )


@define
class MMOCRInferenceConfig(MMDetectionInferenceConfig):
    """Config for the inference process."""

    __related_strict__ = True

    # Whether the output polygon should be formatted to represent a rect, or
    # the polygon should be kept as it is
    to_rect_polygon: bool = related.BooleanField(default=False, required=False)


@define
class MMOCRConfig(MMDetectionConfig):
    """Config for the MMOCR model."""

    __related_strict__ = True

    __text_class_id__ = 0
    __text_class_name__ = "text"

    inference_config: MMOCRInferenceConfig = related.ChildField(
        cls=MMOCRInferenceConfig
    )

    train_config: MMOCRTrainConfig = related.ChildField(cls=MMOCRTrainConfig)

    class_mapping: ClassMappingConfig = related.ChildField(
        cls=ClassMappingConfig,
        default=ClassMappingConfig(
            mapping=[],
            model_classes=[
                # OCR models only detect text, therefore it has this default class mapping
                ClassMappingModelClassesConfig(
                    class_id=__text_class_id__,
                    class_name=__text_class_name__,
                )
            ],
            number_model_classes=1,
        ),
    )
    mmdeploy_onnxruntime_config: Optional[MMDetectionMMDeployOnnxruntimeConfig] = (
        related.ChildField(
            cls=MMDetectionMMDeployOnnxruntimeConfig, default=None, required=False
        )
    )

    mmdeploy_onnxruntime_float16_config: Optional[
        MMDetectionMMDeployOnnxruntimeConfig
    ] = related.ChildField(
        cls=MMDetectionMMDeployOnnxruntimeConfig, default=None, required=False
    )

    mmdeploy_tensorrt_config: Optional[MMDetectionMMDeployTensorRTConfig] = (
        related.ChildField(
            cls=MMDetectionMMDeployTensorRTConfig, default=None, required=False
        )
    )

    mm_config: MMConfig = related.ChildField(
        cls=MMConfig, default=Factory(MMConfig), required=False
    )
