# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for RobustScanner model."""

import logging
import os
from pathlib import Path
from typing import Dict, List

import pytest
from mlcvzoo_base.api.data.ocr_perception import OCRPerception
from mlcvzoo_base.api.structs import Runtime

from mlcvzoo_mmocr.tests.unit_tests.conftest import (
    unload_modules,
    verify_mmdeploy_configs,
    verify_ocr_perceptions,
)

logger = logging.getLogger(__name__)


@pytest.fixture(name="expected_predictions")
def expected_predictions_fixture() -> List[OCRPerception]:
    """Provide a list of expected predictions."""

    return [
        OCRPerception(
            score=0.9970841833523342,
            content="MLCVZOO",
        ),
    ]


@pytest.mark.parametrize(
    "unload_modules",
    [["mmdeploy", "mmcv", "mmdet", "mmengine", "mmocr", "mlcvzoo"]],
    indirect=True,
)
def test_inference(
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    text_recognition_test_image_path: Path,
    expected_predictions: List[OCRPerception],
    unload_modules,
) -> None:
    """Test that predictions are correct on a test image."""
    config_path = (
        test_data_root
        / "robustscanner"
        / "robustscanner_r31_academic_mmdeploy_static_config_dict.yaml"
    )
    from mlcvzoo_mmocr.text_recognition_model import MMOCRTextRecognitionModel

    model = MMOCRTextRecognitionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    _, predictions = model.predict(data_item=str(text_recognition_test_image_path))
    predictions_many = model.predict_many(
        data_items=[
            str(text_recognition_test_image_path),
            str(text_recognition_test_image_path),
        ]
    )

    for predictions in (predictions, predictions_many[0][1], predictions_many[1][1]):
        verify_ocr_perceptions(
            actual_perceptions=predictions,
            expected_perceptions=expected_predictions,
            score_tolerance=2e-8,
        )


@pytest.mark.parametrize(
    "unload_modules",
    [["mmdeploy", "mmcv", "mmdet", "mmengine", "mmocr", "mlcvzoo"]],
    indirect=True,
)
def test_training(
    project_root: Path,
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    text_recognition_test_image_path: str,
    unload_modules,
) -> None:
    """Test that a training is working."""
    from mlcvzoo_mmocr.text_recognition_model import MMOCRTextRecognitionModel

    config_path = test_data_root / "robustscanner" / "robustscanner_custom.yaml"

    model = MMOCRTextRecognitionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=False,
    )
    try:
        model.train()

    except RuntimeError as re:
        del model
        if "CUDA out of memory" in str(re):
            pytest.skip(
                "Could not test training of RobustScanner (MMOCR). GPU memory is to small"
            )
        else:
            raise re

    best_metric_info, metrics = model.determine_training_metrics()
    training_logs = model.determine_training_logs()

    assert best_metric_info.name == "val/recog/word_acc"

    train_metric_names = [
        "train/lr",
        "train/data_time",
        "train/loss",
        "train/loss_ce",
        "train/time",
        "train/iter",
        "train/memory",
        "train/step",
    ]

    val_names_metrics = [
        "val/recog/word_acc",
        "val/recog/word_acc_ignore_case",
        "val/recog/word_acc_ignore_case_symbol",
        "val/recog/char_recall",
        "val/recog/char_precision",
        "val/data_time",
        "val/time",
    ]

    # Depending on the configuration of max-epoch and val-interval, we
    # get not all metrics for every epoch. This is what we expect to
    # be contained using the configuration of the test model.
    expected_simplified_logs: Dict[int, List[str]] = {
        1: train_metric_names,
        2: train_metric_names,
        3: train_metric_names + val_names_metrics,
        4: train_metric_names,
        5: train_metric_names,
        6: train_metric_names + val_names_metrics,
        7: train_metric_names + val_names_metrics,
    }

    produced_simplified_logs = {
        epoch: list(metrics.keys()) for epoch, metrics in training_logs.items()
    }

    assert produced_simplified_logs == expected_simplified_logs

    del model

    model = MMOCRTextRecognitionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    model.restore(
        checkpoint_path=str(
            project_root
            / "test_output"
            / "robustscanner_custom"
            / "robustscanner_custom_epoch_0003.pth",
        )
    )

    # Only check if a prediction does not raise an error
    model.predict(data_item=str(text_recognition_test_image_path))


@pytest.mark.parametrize(
    "runtime,score_tolerance",
    [
        (
            Runtime.ONNXRUNTIME,
            7e-2,
        ),
        (
            Runtime.ONNXRUNTIME_FLOAT16,
            7e-2,
        ),
        pytest.param(
            Runtime.TENSORRT,
            7e-2,
            marks=pytest.mark.xfail(
                # See https://github.com/lucas-camp/mmdeploy/commit/d08b8af2a350053f81ac8dbce1d024e1fb846d0e for a possible fix in MMDeploy
                reason="RobustScanner model cannot be converted to TensorRT."
            ),
        ),
    ],
    ids=["ONNXRUNTIME", "ONNXRUNTIME_FLOAT16", "TENSORRT"],
)
def test_mmdeploy_inference(
    project_root: Path,
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    text_recognition_test_image_path: str,
    expected_predictions: List[OCRPerception],
    runtime: str,
    score_tolerance: float,
) -> None:
    """Test that predictions are correct on a test image for an MMDeploy converted model."""
    from mlcvzoo_mmocr.text_recognition_model import MMOCRTextRecognitionModel

    output_base_path = (
        project_root
        / "test_output"
        / "robustscanner"
        / "mmdeploy"
        / "static"
        / f"{runtime.lower()}"
    )

    config_path = (
        test_data_root
        / "robustscanner"
        / "robustscanner_r31_academic_mmdeploy_static_config_dict.yaml"
    )

    model = MMOCRTextRecognitionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=runtime,
    )

    _, predictions = model.predict(str(text_recognition_test_image_path))
    predictions_many = model.predict_many(
        [
            str(text_recognition_test_image_path),
            str(text_recognition_test_image_path),
        ]
    )

    assert Path(
        getattr(
            model.configuration, f"mmdeploy_{runtime.lower()}_config"
        ).checkpoint_path
    ).exists()

    for dump_info_file in ["deploy.json", "detail.json", "pipeline.json"]:
        assert (output_base_path / dump_info_file).exists()

    for predictions in (predictions, predictions_many[0][1], predictions_many[1][1]):
        verify_ocr_perceptions(
            actual_perceptions=predictions,
            expected_perceptions=expected_predictions,
            score_tolerance=score_tolerance,
        )


@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_mmdeploy_configs(
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    runtime: str,
) -> None:
    """Test that the Python and the dict configs are equal."""
    from mlcvzoo_mmocr.text_recognition_model import MMOCRTextRecognitionModel

    py_config_path = (
        test_data_root
        / "robustscanner"
        / "robustscanner_r31_academic_mmdeploy_static_config_path.yaml"
    )
    dict_config_path = (
        test_data_root
        / "robustscanner"
        / "robustscanner_r31_academic_mmdeploy_static_config_dict.yaml"
    )

    verify_mmdeploy_configs(
        py_config_path=py_config_path,
        dict_config_path=dict_config_path,
        model_type=MMOCRTextRecognitionModel,
        string_replacement_map=string_replacement_map,
        runtime=runtime,
    )
