# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Definition of common pytest fixtures for testing."""

import gc
import json
import os
import shutil
from pathlib import Path
from sys import modules
from typing import Dict, Generator, List, Type, Union

import numpy as np
import pytest
from config_builder.replacement_map import (
    get_current_replacement_map,
    set_replacement_map_value,
    update_replacement_map_from_os,
)
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.ocr_perception import OCRPerception
from mlcvzoo_base.api.data.segmentation import Segmentation
from mlcvzoo_base.api.structs import Runtime
from mlcvzoo_base.configuration.replacement_config import ReplacementConfig
from mlcvzoo_mmdetection.utils import init_mm_config

import mlcvzoo_mmocr
from mlcvzoo_mmocr.text_detection_model import MMOCRTextDetectionModel
from mlcvzoo_mmocr.text_recognition_model import MMOCRTextRecognitionModel


@pytest.fixture(name="project_root")
def project_root_fixture() -> Path:
    """Provide the project root path."""

    this_dir = Path(os.path.dirname(os.path.abspath(__file__))).resolve()

    setup_path = this_dir
    while setup_path.exists() and setup_path.name != mlcvzoo_mmocr.__name__:
        if setup_path == setup_path.parent:
            raise RuntimeError("Could not find setup_path!")
        else:
            setup_path = setup_path.parent
    # One more to be above the target directory
    setup_path = setup_path.parent

    return setup_path


@pytest.fixture(name="cleanup", scope="function", autouse=True)
def cleanup_fixture(project_root: Path) -> Generator[None, None, None]:
    """Removes the data_output directory after the test is run."""

    yield  # Test runs now

    shutil.rmtree(project_root / "test_output", ignore_errors=True)


@pytest.fixture(name="test_data_root")
def test_data_root_fixture(project_root: Path) -> Path:
    """Provide path to the test data root directory."""

    return project_root / "test_data" / "test_mmocr" / "config"


@pytest.fixture(name="string_replacement_map")
def string_replacement_map_fixture(project_root: Path) -> Dict[str, str]:
    """Provide string replacement map for tests."""

    set_replacement_map_value(ReplacementConfig.PROJECT_ROOT_DIR_KEY, str(project_root))

    set_replacement_map_value(
        "MMOCR_DIR",
        str((project_root / ".." / ".." / "mmocr").resolve()),
    )
    set_replacement_map_value(
        "MMDEPLOY_DIR",
        str((project_root / ".." / ".." / "mmdeploy").resolve()),
    )
    set_replacement_map_value(
        "BASELINE_MODEL_DIR",
        str((project_root / ".." / ".." / "modeling_data").resolve()),
    )
    update_replacement_map_from_os()

    return get_current_replacement_map()


@pytest.fixture(scope="function")
def unload_modules(request):
    """Unloads all given modules and its children and restores them after the test."""

    modules_to_unload = request.param

    def __clean_sys_modules() -> None:
        for sys_module in list(modules.keys()):
            if any(
                [sys_module.startswith(mm_module) for mm_module in modules_to_unload]
            ):
                del modules[sys_module]

    modules_to_reload = {}

    for sys_module in list(modules.keys()):
        if any([sys_module.startswith(module) for module in modules_to_unload]):
            modules_to_reload[sys_module] = modules[sys_module]

    __clean_sys_modules()

    yield

    __clean_sys_modules()

    for module_key, module in modules_to_reload.items():
        modules[module_key] = module


@pytest.fixture(name="text_detection_test_image_path")
def text_detection_test_image_path_fixture(project_root: Path) -> Path:
    """Provide path to a text recognition test image."""

    image_path = (
        project_root
        / "test_data"
        / "images"
        / "test_inference_task"
        / "test_text-detection_inference_image.jpg"
    )

    return image_path


@pytest.fixture(name="text_recognition_test_image_path")
def text_recognition_test_image_path_fixture(project_root: Path) -> Path:
    """Provide path to a text recognition test image with text."""

    image_path = (
        project_root
        / "test_data"
        / "images"
        / "test_inference_task"
        / "test_text-recognition_inference_image.jpg"
    )

    return image_path


def max_box_difference(box_1: Box, box_2: Box) -> float:
    """Compute the maximum difference of the coordinates of the corners of two rectangular boxes.

    Args:
        box_1 (Box): The first box.
        box_2 (Box): The second box.

    Returns:
        float: Maximum difference of the coordinates of the corners.
    """

    return float(np.max(np.abs(np.array(box_1.to_list()) - np.array(box_2.to_list()))))


def verify_segmentations(
    actual: List[Segmentation],
    expected: List[Segmentation],
    score_tolerance: float = 0.0,
    box_tolerance: int = 0,
    skip_polygon: bool = False,
):
    """Assert that the actual list of segmentations matches the expected.

    Args:
        actual (List[Segmentation]): List of actual segmentations.
        expected (List[Segmentation]): List of expected segmentations.
        score_tolerance (float, optional): Absolute score tolerance. Defaults to 0.0.
        box_tolerance (int, optional): Absolute tolerance for coordinates of the corners of the
            polygons box. Defaults to 0.
        skip_polygon (bool, optional): If True, do not assert eqaulity of the polygon. Defaults to
            False.
    """

    def error_message(i: int, field: str) -> str:
        """Generate an error message for an segmentation mismatch.

        Args:
            i (int): The index of the segmentation.
            field (str): The name of the attribute, that does not match.

        Returns:
            str: The generated error message.
        """

        return f"Segmentation {i} mismatching '{field}'"

    assert len(actual) == len(expected), "The number of segmentation do not match."

    for i, (actual_seg, expected_seg) in enumerate(zip(actual, expected)):

        assert (
            actual_seg.class_identifier.class_id
            == expected_seg.class_identifier.class_id
        ), error_message(i, "class_identifier.class_id")
        assert (
            actual_seg.class_identifier.class_name
            == expected_seg.class_identifier.class_name
        ), error_message(i, "class_identifier.class_name")

        assert actual_seg.score == pytest.approx(
            expected_seg.score,
            abs=score_tolerance,
            rel=0.0,
        ), error_message(i, "score")

        assert actual_seg.difficult == expected_seg.difficult, error_message(
            i, "difficult"
        )

        assert actual_seg.occluded == expected_seg.occluded, error_message(
            i, "occluded"
        )

        assert actual_seg.content == expected_seg.content, error_message(i, "content")

        assert (
            max_box_difference(
                actual_seg.box(),
                expected_seg.box(),
            )
            <= box_tolerance
        ), error_message(i, "box")

        if not skip_polygon:
            assert np.all(
                actual_seg.polygon() == expected_seg.polygon()
            ), error_message(i, "polygon")


def verify_ocr_perceptions(
    actual_perceptions: List[OCRPerception],
    expected_perceptions: List[OCRPerception],
    score_tolerance: float = 0.0,
):
    """Assert that the actual list of OCR perceptions matches the expected.

    Args:
        actual_perceptions (List[OCRPerception]): List of actual OCR perceptions.
        expected_perceptions (List[OCRPerception]): List of expected OCR perceptions.
        score_tolerance (float, optional): Absolute score tolerance. Defaults to 0.0.
    """

    def error_message(i: int, field: str) -> str:
        """Generate an error message for an OCR perception mismatch.

        Args:
            i (int): The index of the OCR perception.
            field (str): The name of the attribute, that does not match.

        Returns:
            str: The generated error message.
        """

        return f"OCRPerception {i} mismatching '{field}'"

    assert len(actual_perceptions) == len(
        expected_perceptions
    ), "The number of OCR perceptions do not match."

    for i, (actual_perception, expected_perception) in enumerate(
        zip(actual_perceptions, expected_perceptions)
    ):
        assert actual_perception.score == pytest.approx(
            expected_perception.score,
            abs=score_tolerance,
            rel=0.0,
        ), error_message(i, "score")

        assert actual_perception.content == expected_perception.content, error_message(
            i, "content"
        )


def verify_mmdeploy_configs(
    py_config_path: Path,
    dict_config_path: Path,
    model_type: Union[Type[MMOCRTextDetectionModel], Type[MMOCRTextRecognitionModel]],
    string_replacement_map: Dict[str, str],
    runtime: str,
) -> None:
    """Test that the MMDeploy Python and the dict configs are equal.

    Args:
        py_config_path (Path): Path to the MMDeploy Python config.
        dict_config_path (Path): Path to the MMDeploy dict config.
        model_type (Union[Type[MMOCRTextDetectionModel], Type[MMOCRTextRecognitionModel]]): The
            model type.
        string_replacement_map (Dict[str, str]): The string replacement map.
        runtime (str): The runtime.
    """

    py_model = model_type(
        from_yaml=str(py_config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )
    dict_model = model_type(
        from_yaml=str(dict_config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )

    py_model_cfg_dict = (
        py_model.cfg._cfg_dict  # pylint: disable=protected-access,no-member
    )
    dict_model_cfg_dict = (
        dict_model.cfg._cfg_dict  # pylint: disable=protected-access,no-member
    )
    py_mmdelpoy_cfg_dict = init_mm_config(  # pylint: disable=protected-access,no-member
        mm_config=getattr(py_model.configuration, f"mmdeploy_{runtime.lower()}_config")
    )._cfg_dict
    dict_mmdelpoy_cfg_dict = (
        init_mm_config(  # pylint: disable=protected-access,no-member
            mm_config=getattr(
                dict_model.configuration, f"mmdeploy_{runtime.lower()}_config"
            )
        )._cfg_dict
    )

    assert py_model_cfg_dict == dict_model_cfg_dict

    # It doesn't matter if a sequence is a list or a tuple
    # Serializing and desirializing result in lists for both
    assert json.loads(json.dumps(py_mmdelpoy_cfg_dict)) == json.loads(
        json.dumps(dict_mmdelpoy_cfg_dict)
    )
