# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for class MMOCRModel."""

from pathlib import Path
from typing import Dict

import pytest
from mlcvzoo_base.api.structs import Runtime
from pytest_mock import MockerFixture

from mlcvzoo_mmocr.text_detection_model import MMOCRTextDetectionModel


def test_init_inference_model_runtime_not_supported(
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
) -> None:
    """Test that an exception is raised, if _init_inference_model is called for an unsupported
    runtime."""

    config_path = (
        test_data_root
        / "panet"
        / "panet_r18_fpem_ffm_600e_ctw1500"
        / "panet_r18_fpem_ffm_600e_ctw1500_mmdeploy_static_config_dict.yaml"
    )

    model = MMOCRTextDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )

    model.runtime = "INVALID"

    with pytest.raises(
        ValueError,
        match="Initialization for inference is not supported for runtime 'INVALID'.",
    ):
        model._init_inference_model()  # pylint: disable=protected-access


@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_init_inference_model_config_missing(
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    runtime: str,
) -> None:
    """Test that an exception is raised, if _init_inference_model is called for a MMDeploy runtime
    and the config is missing."""

    config_path = (
        test_data_root
        / "panet"
        / "panet_r18_fpem_ffm_600e_ctw1500"
        / "panet_r18_fpem_ffm_600e_ctw1500.yaml"
    )

    model = MMOCRTextDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )

    model.runtime = runtime

    with pytest.raises(
        ValueError,
        match=f"The mmdeploy_{runtime.lower()}_config must be provided for runtime '{runtime}'.",
    ):
        model._init_inference_model()  # pylint: disable=protected-access


@pytest.mark.parametrize(
    "unload_modules",
    [["mmdeploy", "mmcv", "mmdet", "mmengine", "mmocr", "mlcvzoo"]],
    indirect=True,
)
@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_init_for_inference_fails_if_mmdeploy_not_found(
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    runtime: str,
    unload_modules,
    mocker: MockerFixture,
):
    """Test that an exception is raised, if _init_for_inference is called for a MMDeploy runtime
    and module mmdeploy is not found."""

    config_path = (
        test_data_root
        / "panet"
        / "panet_r18_fpem_ffm_600e_ctw1500"
        / "panet_r18_fpem_ffm_600e_ctw1500_mmdeploy_static_config_dict.yaml"
    )

    mocker.patch.dict("sys.modules", {"mmdeploy": None})

    with pytest.raises(
        RuntimeError,
        match="Extra 'mmdeploy' must be installed to run a model which is deployed with "
        "MMDeploy.",
    ):
        from mlcvzoo_mmocr.text_detection_model import MMOCRTextDetectionModel

        model = MMOCRTextDetectionModel(
            from_yaml=str(config_path),
            string_replacement_map=string_replacement_map,
            init_for_inference=True,
            runtime=Runtime.DEFAULT,
        )

        model.runtime = runtime

        model._init_inference_model()  # pylint: disable=protected-access
