# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for class MMOCRTextRecognitionModel."""

from pathlib import Path
from typing import Dict

import pytest
from mlcvzoo_base.api.structs import Runtime
from pytest_mock import MockerFixture

from mlcvzoo_mmocr.text_recognition_model import MMOCRTextRecognitionModel


@pytest.fixture(name="mmdeploy_robustscanner_config_path")
def mmdeploy_robustscanner_config_path_fixture(test_data_root: Path) -> Path:
    """Provide path to MMDeploy RobustScanner configuration file."""

    return (
        test_data_root
        / "robustscanner"
        / "robustscanner_r31_academic_mmdeploy_static_config_path.yaml"
    )


@pytest.mark.parametrize(
    "prediction_method_name,error_prefix",
    [
        ("predict", "Prediction"),
        ("predict_many", "Multi-prediction"),
    ],
    ids=["predict", "predict_many"],
)
def test_prediction_runtime_not_supported(
    mmdeploy_robustscanner_config_path: Path,
    string_replacement_map: Dict[str, str],
    prediction_method_name: str,
    error_prefix: str,
) -> None:
    """Test that an exception is raised, if a model prediction is performed and the runtime is
    not supported."""

    model = MMOCRTextRecognitionModel(
        from_yaml=str(mmdeploy_robustscanner_config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )

    model.runtime = "INVALID"

    with pytest.raises(
        ValueError, match=f"{error_prefix} is not supported for runtime 'INVALID'."
    ):
        prediction_method = getattr(model, prediction_method_name)
        prediction_method(None)


@pytest.mark.parametrize(
    "prediction_method_name",
    [
        "predict",
        "predict_many",
    ],
)
def test_prediction_no_net(
    mmdeploy_robustscanner_config_path: Path,
    string_replacement_map: Dict[str, str],
    prediction_method_name: str,
) -> None:
    """Test that an exception is raised, if a model prediction is performed and the net attribute is
    None."""

    model = MMOCRTextRecognitionModel(
        from_yaml=str(mmdeploy_robustscanner_config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )

    model.net = None

    with pytest.raises(
        ValueError,
        match="The 'net' attribute is not initialized, make sure to instantiate with init_for_inference=True",
    ):
        prediction_method = getattr(model, prediction_method_name)
        prediction_method(None)


@pytest.mark.parametrize(
    "prediction_method_name",
    [
        "predict",
        "predict_many",
    ],
)
@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.DEFAULT,
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_prediction_no_inferencer(
    mmdeploy_robustscanner_config_path: Path,
    string_replacement_map: Dict[str, str],
    prediction_method_name: str,
    runtime: str,
) -> None:
    """Test that an exception is raised, if a model prediction is performed and there is no
    inferencer."""

    model = MMOCRTextRecognitionModel(
        from_yaml=str(mmdeploy_robustscanner_config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )

    model.runtime = runtime
    model.inferencer = None

    with pytest.raises(
        ValueError,
        match="The 'inferencer' attribute is not initialized, make sure to instantiate with "
        "init_for_inference=True",
    ):
        prediction_method = getattr(model, prediction_method_name)
        prediction_method(None)


@pytest.mark.parametrize(
    "prediction_method_name",
    [
        "predict",
        "predict_many",
    ],
)
@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_prediction_fails_if_mmdeploy_not_found(
    mmdeploy_robustscanner_config_path: Path,
    string_replacement_map: Dict[str, str],
    prediction_method_name: str,
    runtime: str,
    mocker: MockerFixture,
):
    """Test that an exception is raised, if a model prediction is performed and the
    module mmdeploy is not found."""

    mocker.patch("mlcvzoo_mmdetection.model.MMDeployConverter")
    mocker.patch("mlcvzoo_mmocr.model.MMDeployInferencer")

    model = MMOCRTextRecognitionModel(
        from_yaml=str(mmdeploy_robustscanner_config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=runtime,
    )

    mocker.patch(
        "mlcvzoo_mmocr.model.MMDeployInferencer",
        None,
    )

    with pytest.raises(
        RuntimeError,
        match="Extra 'mmdeploy' must be installed to run a model which is deployed with "
        "MMDeploy.",
    ):
        prediction_method = getattr(model, prediction_method_name)
        prediction_method(None)
