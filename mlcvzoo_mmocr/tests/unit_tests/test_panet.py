# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for PANet model."""

from pathlib import Path
from typing import Dict, List

import pytest
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.data.segmentation import Segmentation
from mlcvzoo_base.api.structs import Runtime

from mlcvzoo_mmocr.configuration import MMOCRConfig
from mlcvzoo_mmocr.tests.unit_tests.conftest import (
    verify_mmdeploy_configs,
    verify_segmentations,
)
from mlcvzoo_mmocr.text_detection_model import MMOCRTextDetectionModel


@pytest.fixture(name="expected_predictions")
def expected_predictions_fixture() -> List[Segmentation]:
    """Provide a list of expected segmentations."""

    return [
        Segmentation(
            polygon=[
                [107.20186697782965, 211.25],
                [103.95332555425905, 214.5],
                [103.95332555425905, 217.75],
                [107.20186697782965, 221.0],
                [107.20186697782965, 230.75],
                [103.95332555425905, 234.0],
                [103.95332555425905, 279.5],
                [107.20186697782965, 282.75],
                [103.95332555425905, 286.0],
                [133.1901983663944, 286.0],
                [136.438739789965, 289.25],
                [175.42123687281216, 289.25],
                [178.66977829638276, 292.5],
                [207.9066511085181, 292.5],
                [211.1551925320887, 295.75],
                [253.38623103850645, 295.75],
                [256.634772462077, 292.5],
                [331.3512252042007, 292.5],
                [334.5997666277713, 289.25],
                [367.08518086347726, 289.25],
                [370.33372228704786, 292.5],
                [497.0268378063011, 292.5],
                [500.2753792298717, 295.75],
                [500.2753792298717, 299.0],
                [490.5297549591599, 308.75],
                [487.2812135355893, 308.75],
                [490.5297549591599, 308.75],
                [493.7782963827305, 312.0],
                [490.5297549591599, 315.25],
                [484.0326721120187, 315.25],
                [480.7841306884481, 318.5],
                [477.5355892648775, 318.5],
                [477.5355892648775, 325.0],
                [480.7841306884481, 325.0],
                [484.0326721120187, 328.25],
                [487.2812135355893, 328.25],
                [493.7782963827305, 334.75],
                [497.0268378063011, 334.75],
                [503.5239206534423, 341.25],
                [506.7724620770129, 341.25],
                [510.0210035005835, 344.5],
                [506.7724620770129, 347.75],
                [503.5239206534423, 347.75],
                [500.2753792298717, 351.0],
                [500.2753792298717, 357.5],
                [584.7374562427071, 357.5],
                [587.9859976662777, 354.25],
                [591.2345390898483, 354.25],
                [594.4830805134189, 351.0],
                [594.4830805134189, 338.0],
                [597.7316219369895, 334.75],
                [597.7316219369895, 325.0],
                [600.9801633605601, 321.75],
                [600.9801633605601, 308.75],
                [581.4889148191365, 289.25],
                [581.4889148191365, 286.0],
                [578.2403733955659, 282.75],
                [578.2403733955659, 227.5],
                [574.9918319719953, 224.25],
                [574.9918319719953, 221.0],
                [571.7432905484247, 217.75],
                [568.4947491248541, 217.75],
                [565.2462077012837, 214.5],
                [536.0093348891482, 214.5],
                [532.7607934655776, 211.25],
                [500.2753792298717, 211.25],
                [497.0268378063011, 214.5],
                [487.2812135355893, 214.5],
                [484.0326721120187, 211.25],
                [474.2870478413069, 211.25],
                [471.03850641773636, 214.5],
                [467.78996499416576, 211.25],
            ],
            class_identifier=ClassIdentifier(
                MMOCRConfig.__text_class_id__,
                class_name=MMOCRConfig.__text_class_name__,
            ),
            score=0.9622833728790283,
            difficult=False,
            occluded=False,
            content="",
        )
    ]


@pytest.fixture(name="expected_predictions_rect")
def expected_predictions_rect_fixture() -> List[Segmentation]:
    """Provide a list of expected rectangular predictions."""

    return [
        Segmentation(
            polygon=[
                [103.0, 211.0],
                [600.0, 211.0],
                [600.0, 357.0],
                [103.0, 357.0],
            ],
            class_identifier=ClassIdentifier(
                class_id=MMOCRConfig.__text_class_id__,
                class_name=MMOCRConfig.__text_class_name__,
            ),
            score=0.9622833728790283,
            difficult=False,
            occluded=False,
            content="",
        )
    ]


def test_class_id_dict(
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
) -> None:
    """Test that class id dict is correct."""

    config_path = (
        test_data_root
        / "panet"
        / "panet_r18_fpem_ffm_600e_ctw1500"
        / "panet_r18_fpem_ffm_600e_ctw1500.yaml"
    )

    model = MMOCRTextDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    assert model.num_classes == 1
    assert model.get_classes_id_dict() == {
        MMOCRConfig.__text_class_id__: MMOCRConfig.__text_class_name__
    }


def test_inference(
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    text_detection_test_image_path: str,
    expected_predictions: List[Segmentation],
) -> None:
    """Test that predictions are correct on a test image."""

    config_path = (
        test_data_root
        / "panet"
        / "panet_r18_fpem_ffm_600e_ctw1500"
        / "panet_r18_fpem_ffm_600e_ctw1500.yaml"
    )

    model = MMOCRTextDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    _, predictions = model.predict(data_item=str(text_detection_test_image_path))
    predictions_many = model.predict_many(
        data_items=[
            str(text_detection_test_image_path),
            str(text_detection_test_image_path),
        ]
    )

    for predictions in (predictions, predictions_many[0][1], predictions_many[1][1]):
        verify_segmentations(
            actual=predictions, expected=expected_predictions, score_tolerance=1e-7
        )


def test_inference_rect(
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    text_detection_test_image_path: str,
    expected_predictions_rect: List[Segmentation],
) -> None:
    """Test that predictions are correct on a test image."""

    config_path = (
        test_data_root
        / "panet"
        / "panet_r18_fpem_ffm_600e_ctw1500"
        / "panet_r18_fpem_ffm_600e_ctw1500_rect.yaml"
    )

    model = MMOCRTextDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    _, predictions = model.predict(data_item=str(text_detection_test_image_path))
    predictions_many = model.predict_many(
        data_items=[
            str(text_detection_test_image_path),
            str(text_detection_test_image_path),
        ]
    )

    for predictions in (predictions, predictions_many[0][1], predictions_many[1][1]):
        verify_segmentations(
            actual=predictions, expected=expected_predictions_rect, score_tolerance=1e-7
        )


def test_training(
    project_root: Path,
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    text_detection_test_image_path: str,
) -> None:
    """Test that a training is working."""

    config_path = (
        test_data_root
        / "panet"
        / "panet_r18_fpem_ffm_600e_ctw1500"
        / "panet_r18_fpem_ffm_600e_custom.yaml"
    )

    model = MMOCRTextDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=False,
    )

    try:
        model.train()

        del model
    except RuntimeError as re:
        del model

        if "CUDA out of memory" in str(re):
            pytest.skip(
                "Could not test training of PANet (MMOCR). GPU memory is to small"
            )
        else:
            raise re

    model = MMOCRTextDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    model.restore(
        checkpoint_path=str(
            project_root
            / "test_output"
            / "panet_r18_fpem_ffm_600e_custom"
            / "panet_r18_fpem_ffm_600e_custom_epoch_1.pth",
        )
    )

    # Only check if a prediction does not raise an error
    model.predict(data_item=str(text_detection_test_image_path))


def test_training_distributed(
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
) -> None:
    """Test that a distributed training is working."""

    config_path = (
        test_data_root
        / "panet"
        / "panet_r18_fpem_ffm_600e_ctw1500__distributed"
        / "panet_r18_fpem_ffm_600e_ctw1500__distributed.yaml"
    )

    model = MMOCRTextDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=False,
    )

    try:
        model.train()

        del model
    except RuntimeError as re:
        del model

        if "CUDA out of memory" in str(re):
            pytest.skip(
                "Could not test training of PANet (MMOCR). GPU memory is to small"
            )
        else:
            raise re


@pytest.mark.parametrize(
    "runtime,score_tolerance,box_tolerance,skip_polygon",
    [
        (
            Runtime.ONNXRUNTIME,
            2e-7,
            0,
            False,
        ),
        (
            Runtime.ONNXRUNTIME_FLOAT16,
            4e-5,
            0,
            False,
        ),
        (
            Runtime.TENSORRT,
            3e-4,
            4,
            True,
        ),
    ],
    ids=["ONNXRUNTIME", "ONNXRUNTIME_FLOAT16", "TENSORRT"],
)
def test_mmdeploy_inference(
    project_root: Path,
    test_data_root: str,
    string_replacement_map: Dict[str, str],
    text_detection_test_image_path: str,
    expected_predictions: List[Segmentation],
    runtime: str,
    score_tolerance: float,
    box_tolerance: int,
    skip_polygon: bool,
) -> None:
    """Test that predictions are correct on a test image for an MMDeploy converted model."""

    output_base_path = (
        project_root
        / "test_output"
        / "panet_r18_fpem_ffm_600e_ctw1500"
        / "mmdeploy"
        / "static"
        / f"{runtime.lower()}"
    )

    config_path = (
        test_data_root
        / "panet"
        / "panet_r18_fpem_ffm_600e_ctw1500"
        / "panet_r18_fpem_ffm_600e_ctw1500_mmdeploy_static_config_dict.yaml"
    )

    model = MMOCRTextDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=runtime,
    )

    _, predictions = model.predict(str(text_detection_test_image_path))
    predictions_many = model.predict_many(
        [
            str(text_detection_test_image_path),
            str(text_detection_test_image_path),
        ]
    )

    assert Path(
        getattr(
            model.configuration, f"mmdeploy_{runtime.lower()}_config"
        ).checkpoint_path
    ).exists()

    for dump_info_file in ["deploy.json", "detail.json", "pipeline.json"]:
        assert (output_base_path / dump_info_file).exists()

    for predictions in (predictions, predictions_many[0][1], predictions_many[1][1]):
        verify_segmentations(
            actual=predictions,
            expected=expected_predictions,
            score_tolerance=score_tolerance,
            box_tolerance=box_tolerance,
            skip_polygon=skip_polygon,
        )


@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_mmdeploy_configs(
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    runtime: str,
) -> None:
    """Test that the MMDeploy Python and the dict configs are equal."""

    py_config_path = (
        test_data_root
        / "panet"
        / "panet_r18_fpem_ffm_600e_ctw1500"
        / "panet_r18_fpem_ffm_600e_ctw1500_mmdeploy_static_config_path.yaml"
    )
    dict_config_path = (
        test_data_root
        / "panet"
        / "panet_r18_fpem_ffm_600e_ctw1500"
        / "panet_r18_fpem_ffm_600e_ctw1500_mmdeploy_static_config_dict.yaml"
    )

    verify_mmdeploy_configs(
        py_config_path=py_config_path,
        dict_config_path=dict_config_path,
        model_type=MMOCRTextDetectionModel,
        string_replacement_map=string_replacement_map,
        runtime=runtime,
    )
