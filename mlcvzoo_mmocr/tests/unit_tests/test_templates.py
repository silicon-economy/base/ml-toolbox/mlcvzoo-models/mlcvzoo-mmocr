# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for config templates."""

from pathlib import Path
from typing import Dict, List, Type

from config_builder import BaseConfigClass, ConfigBuilder

from mlcvzoo_mmocr.configuration import MMOCRConfig


def test_config_templates(
    project_root: Path, string_replacement_map: Dict[str, str]
) -> None:
    """Test that the config templates can be loaded."""

    template_path_dict: Dict[Type[BaseConfigClass], List[str]] = {
        MMOCRConfig: [
            str(
                project_root
                / "config"
                / "templates"
                / "models"
                / "mmocr"
                / "panet"
                / "panet_r18_fpem_ffm_600e_ctw1500"
                / "panet_r18_fpem_ffm_600e_ctw1500.yaml"
            )
        ],
    }

    for config_class_type, template_path_list in template_path_dict.items():
        for template_path in template_path_list:
            config_builder = ConfigBuilder(
                class_type=config_class_type,
                yaml_config_path=template_path,
                string_replacement_map=string_replacement_map,
                no_checks=True,
            )

            assert (
                config_builder.configuration is not None
            ), f"config_class_type: {config_class_type}, template_path: {template_path}"
