# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for sR model."""

import os
from pathlib import Path
from typing import Dict, List
from unittest.mock import MagicMock

import pytest
from mlcvzoo_base.api.data.ocr_perception import OCRPerception
from mlcvzoo_base.api.structs import Runtime
from pytest_mock import MockerFixture

from mlcvzoo_mmocr.tests.unit_tests.conftest import (
    verify_mmdeploy_configs,
    verify_ocr_perceptions,
)
from mlcvzoo_mmocr.text_recognition_model import MMOCRTextRecognitionModel


@pytest.fixture(name="expected_predictions")
def expected_predictions_fixture() -> List[OCRPerception]:
    """Provide a list of expected predictions."""

    return [
        OCRPerception(
            score=0.9997635228293282,
            content="MLCVZOO",
        ),
    ]


@pytest.fixture(name="filter_score_mock", scope="function")
def filter_score_mock_fixture(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_mmocr.text_recognition_model.MMOCRTextRecognitionModel._MMOCRTextRecognitionModel__filter_result_score_by_mean",
        return_value=None,
    )


def test_inference(
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    text_recognition_test_image_path: Path,
    expected_predictions: List[OCRPerception],
) -> None:
    """Test that predictions are correct on a test image."""

    config_path = (
        test_data_root
        / "sar"
        / "sar_r31_parallel_decoder_academic"
        / "sar_r31_parallel_decoder_academic.yaml"
    )

    model = MMOCRTextRecognitionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    _, predictions = model.predict(data_item=str(text_recognition_test_image_path))
    predictions_many = model.predict_many(
        data_items=[
            str(text_recognition_test_image_path),
            str(text_recognition_test_image_path),
        ]
    )

    for predictions in (predictions, predictions_many[0][1], predictions_many[1][1]):
        verify_ocr_perceptions(
            actual_perceptions=predictions,
            expected_perceptions=expected_predictions,
            score_tolerance=4e-8,
        )


def test_inference_filtered_by_score(
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    text_recognition_test_image_path: Path,
) -> None:
    """Test that predictions are filtered correctly by score."""

    config_path = (
        test_data_root
        / "sar"
        / "sar_r31_parallel_decoder_academic"
        / "sar_r31_parallel_decoder_academic.yaml"
    )

    model = MMOCRTextRecognitionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    model.configuration.inference_config.score_threshold = 0.9998

    _, predictions = model.predict(data_item=str(text_recognition_test_image_path))
    predictions_many = model.predict_many(
        data_items=[
            str(text_recognition_test_image_path),
            str(text_recognition_test_image_path),
        ]
    )

    for predictions in (predictions, predictions_many[0][1], predictions_many[1][1]):
        assert len(predictions) == 0


def test_batch_inference_filtered_by_score(
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    text_recognition_test_image_path: str,
    filter_score_mock: MagicMock,
) -> None:
    """Test that predictions are filtered correctly by score for a batched model."""

    config_path = (
        test_data_root
        / "sar"
        / "sar_r31_parallel_decoder_academic"
        / "sar_r31_parallel_decoder_academic_batch.yaml"
    )

    model = MMOCRTextRecognitionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    _, predictions = model.predict(data_item=str(text_recognition_test_image_path))
    predictions_many = model.predict_many(
        data_items=[
            str(text_recognition_test_image_path),
            str(text_recognition_test_image_path),
        ]
    )

    for predictions in (predictions, predictions_many[0][1], predictions_many[1][1]):
        assert len(predictions) == 0


@pytest.mark.parametrize(
    "runtime,score_tolerance",
    [
        (
            Runtime.ONNXRUNTIME,
            6e-8,
        ),
        (
            Runtime.ONNXRUNTIME_FLOAT16,
            3e-5,
        ),
        (
            Runtime.TENSORRT,
            6e-8,
        ),
    ],
    ids=["ONNXRUNTIME", "ONNXRUNTIME_FLOAT16", "TENSORRT"],
)
def test_mmdeploy_inference(
    project_root: Path,
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    text_recognition_test_image_path: str,
    expected_predictions: List[OCRPerception],
    runtime: str,
    score_tolerance: float,
) -> None:
    """Test that an MMDeploy deployment is working correctly with a config dict."""

    output_base_path = (
        project_root
        / "test_output"
        / "sar"
        / "sar_r31_parallel_decoder_academic"
        / "mmdeploy"
        / "static"
        / f"{runtime.lower()}"
    )

    # TODO: Remove, when mlcvzoo_mmdetection converter is fixed
    os.makedirs(
        name=output_base_path,
        exist_ok=True,
    )

    config_path = (
        test_data_root
        / "sar"
        / "sar_r31_parallel_decoder_academic"
        / "sar_r31_parallel_decoder_academic_mmdeploy_static_config_dict.yaml"
    )

    model = MMOCRTextRecognitionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=runtime,
    )

    _, predictions = model.predict(str(text_recognition_test_image_path))
    predictions_many = model.predict_many(
        [
            str(text_recognition_test_image_path),
            str(text_recognition_test_image_path),
        ]
    )

    assert Path(
        getattr(
            model.configuration, f"mmdeploy_{runtime.lower()}_config"
        ).checkpoint_path
    ).exists()

    for dump_info_file in ["deploy.json", "detail.json", "pipeline.json"]:
        assert (output_base_path / dump_info_file).exists()

    for predictions in (predictions, predictions_many[0][1], predictions_many[1][1]):
        verify_ocr_perceptions(
            actual_perceptions=predictions,
            expected_perceptions=expected_predictions,
            score_tolerance=score_tolerance,
        )


@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_mmdeploy_configs(
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    runtime: str,
) -> None:
    """Test that the Python and the dict configs are equal."""

    py_config_path = (
        test_data_root
        / "sar"
        / "sar_r31_parallel_decoder_academic"
        / "sar_r31_parallel_decoder_academic_mmdeploy_static_config_path.yaml"
    )
    dict_config_path = (
        test_data_root
        / "sar"
        / "sar_r31_parallel_decoder_academic"
        / "sar_r31_parallel_decoder_academic_mmdeploy_static_config_dict.yaml"
    )

    verify_mmdeploy_configs(
        py_config_path=py_config_path,
        dict_config_path=dict_config_path,
        model_type=MMOCRTextRecognitionModel,
        string_replacement_map=string_replacement_map,
        runtime=runtime,
    )
