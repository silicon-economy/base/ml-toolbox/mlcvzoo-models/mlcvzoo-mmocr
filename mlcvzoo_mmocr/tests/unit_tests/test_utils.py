# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import os

import pytest
from mlcvzoo_mmdetection.utils import extract_logs, parse_logs


def test_extract_logs(project_root) -> None:
    logs_path = os.path.join(project_root, "test_data/metrics/abinet_metrics.json")

    mlcvzoo_logs = extract_logs(logs=parse_logs(file_path=logs_path))

    assert mlcvzoo_logs == {
        1: {
            "train/lr": 0.00010000000000000002,
            "train/data_time": 0.04978287220001221,
            "train/loss": 9.668221950531006,
            "train/loss_visual": 2.7856542766094208,
            "train/loss_lang": 3.5835105776786804,
            "train/loss_fusion": 3.299056887626648,
            "train/time": 0.4974854588508606,
            "train/iter": 4,
            "train/memory": 875,
            "train/step": 4,
            "val/recog/char_recall": 0.0,
            "val/recog/char_precision": 0.0,
            "val/data_time": 0.08845371007919312,
            "val/time": 0.16708135604858398,
        },
        2: {
            "train/lr": 0.00010000000000000002,
            "train/data_time": 0.027930498123168945,
            "train/loss": 7.083658576011658,
            "train/loss_visual": 2.1413841545581818,
            "train/loss_lang": 2.5223855078220367,
            "train/loss_fusion": 2.419888809323311,
            "train/time": 0.32292264699935913,
            "train/iter": 8,
            "train/memory": 875,
            "train/step": 8,
            "val/recog/char_recall": 0.0,
            "val/recog/char_precision": 0.0,
            "val/data_time": 0.012192487716674805,
            "val/time": 0.06986379623413086,
        },
        3: {
            "train/lr": 0.00010000000000000002,
            "train/data_time": 0.004442930221557617,
            "train/loss": 4.9750659465789795,
            "train/loss_visual": 1.535736858844757,
            "train/loss_lang": 1.6970743536949158,
            "train/loss_fusion": 1.742254626750946,
            "train/time": 0.14417943954467774,
            "train/iter": 12,
            "train/memory": 875,
            "train/step": 12,
            "val/recog/char_recall": 0.0,
            "val/recog/char_precision": 0.0,
            "val/data_time": 0.019705867767333983,
            "val/time": 0.0657801628112793,
        },
        4: {
            "train/lr": 0.00010000000000000002,
            "train/data_time": 0.0035588741302490234,
            "train/loss": 3.7310215711593626,
            "train/loss_visual": 1.2961244344711305,
            "train/loss_lang": 1.152434492111206,
            "train/loss_fusion": 1.2824626326560975,
            "train/time": 0.14348015785217286,
            "train/iter": 16,
            "train/memory": 875,
            "train/step": 16,
            "val/recog/char_recall": 0.0,
            "val/recog/char_precision": 0.0,
            "val/data_time": 0.02706036567687988,
            "val/time": 0.08572778701782227,
        },
    }
