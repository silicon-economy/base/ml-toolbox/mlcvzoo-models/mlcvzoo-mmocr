# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import logging
import os
from pathlib import Path
from typing import Dict, List

import pytest
import torch
from mlcvzoo_base.api.data.ocr_perception import OCRPerception
from mlcvzoo_base.api.structs import Runtime

from mlcvzoo_mmocr.tests.unit_tests.conftest import verify_ocr_perceptions
from mlcvzoo_mmocr.text_recognition_model import MMOCRTextRecognitionModel

logger = logging.getLogger(__name__)
logging.getLogger("matplotlib.font_manager").disabled = True


@pytest.fixture(name="expected_predictions")
def expected_predictions_fixture() -> List[OCRPerception]:
    """Provide a list of expected predictions."""

    return [
        OCRPerception(
            score=0.9581230112484523,
            content="mlcvzoo",
        ),
    ]


def test_abinet_inference(
    project_root: Path,
    string_replacement_map: Dict[str, str],
    expected_predictions: List[OCRPerception],
) -> None:
    mmocr_model = MMOCRTextRecognitionModel(
        from_yaml=os.path.join(
            project_root,
            "test_data/test_mmocr/config/abinet/abinet_20e_st-an_mj/abinet_20e_st-an_mj.yaml",
        ),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    test_image_path_0 = os.path.join(
        project_root,
        "test_data/images/test_inference_task/test_text-recognition_inference_image.jpg",
    )

    _, ocr_perceptions_0 = mmocr_model.predict(data_item=test_image_path_0)

    logger.debug(
        "\n==============================================================\n"
        "PREDICTED OCR-Perceptions: \n"
        " %s\n"
        "\n==============================================================\n",
        ocr_perceptions_0,
    )

    verify_ocr_perceptions(
        actual_perceptions=ocr_perceptions_0,
        expected_perceptions=expected_predictions,
        score_tolerance=6e-5,
    )


def test_abinet_train(
    project_root: Path,
    string_replacement_map: Dict[str, str],
) -> None:
    mmocr_model = MMOCRTextRecognitionModel(
        from_yaml=os.path.join(
            project_root,
            "test_data/test_mmocr/config/abinet/abinet_20e_st-an_mj/abinet_custom_config_dict.yaml",
        ),
        string_replacement_map=string_replacement_map,
        init_for_inference=False,
    )

    mmocr_model.train()

    assert os.path.isfile(
        project_root
        / "test_output"
        / "abinet_20e_st-an_mj"
        / "abinet_20e_st-an_mj_epoch_0003.pth",
    )


@pytest.mark.parametrize(
    "runtime,score_tolerance",
    [
        (
            Runtime.DEFAULT,
            6e-5,
        ),
        (
            Runtime.ONNXRUNTIME,
            0.05,
        ),
        (
            Runtime.ONNXRUNTIME_FLOAT16,
            0.0015,
        ),
        (
            Runtime.TENSORRT,
            6e-5,
        ),
    ],
    ids=["DEFAULT", "ONNXRUNTIME", "ONNXRUNTIME_FLOAT16", "TENSORRT"],
)
def test_mmdeploy_inference(
    project_root: Path,
    test_data_root: Path,
    string_replacement_map: Dict[str, str],
    text_recognition_test_image_path: Path,
    expected_predictions: List[OCRPerception],
    runtime: str,
    score_tolerance: float,
) -> None:
    """Test that an MMDeploy deployment is working correctly with a config dict."""

    output_base_path = (
        project_root
        / "test_output"
        / "abinet_20e_st-an_mj"
        / "mmdeploy"
        / "static"
        / f"{runtime.lower()}"
    )

    config_path = (
        test_data_root
        / "abinet"
        / "abinet_20e_st-an_mj"
        / "abinet_20e_st-an_mj_config_dict.yaml"
    )

    try:
        model = MMOCRTextRecognitionModel(
            from_yaml=str(config_path),
            string_replacement_map=string_replacement_map,
            init_for_inference=True,
            runtime=runtime,
        )

        _, predictions = model.predict(str(text_recognition_test_image_path))

        predictions_many = model.predict_many(
            [
                str(text_recognition_test_image_path),
                str(text_recognition_test_image_path),
            ]
        )

        for predictions in (
            predictions,
            predictions_many[0][1],
            predictions_many[1][1],
        ):
            verify_ocr_perceptions(
                actual_perceptions=predictions,
                expected_perceptions=expected_predictions,
                score_tolerance=score_tolerance,
            )

        if runtime != "DEFAULT":
            assert Path(
                getattr(
                    model.configuration, f"mmdeploy_{runtime.lower()}_config"
                ).checkpoint_path
            ).exists()

            for dump_info_file in ["deploy.json", "detail.json", "pipeline.json"]:
                assert (output_base_path / dump_info_file).exists()
    except torch.onnx.errors.UnsupportedOperatorError:
        pytest.skip("Unsupported operator, skip test for this model")
