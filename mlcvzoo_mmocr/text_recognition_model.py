# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Module for defining the model classes that are used to wrap the mmocr framework.
"""

import logging
import os
from statistics import mean
from typing import Dict, List, Optional, Union

from mlcvzoo_base.api.data.ocr_perception import OCRPerception
from mlcvzoo_base.api.data.types import ImageType
from mlcvzoo_base.api.model import OCRModel
from mlcvzoo_base.api.structs import Runtime
from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig
from mlcvzoo_base.data_preparation.annotation_handler import AnnotationHandler
from mlcvzoo_base.data_preparation.annotation_writer.txt_annotation_writer import (
    TextCropAnnotationWriter,
)
from mlcvzoo_mmdetection.model import MMDetectionModel
from mmocr.apis.inferencers import TextRecInferencer
from mmocr.structures.textrecog_data_sample import TextRecogDataSample

from mlcvzoo_mmocr.configuration import MMOCRConfig
from mlcvzoo_mmocr.model import MMOCRModel

logger = logging.getLogger(__name__)


class MMOCRTextRecognitionModel(
    MMOCRModel[OCRPerception, TextRecInferencer, TextRecogDataSample],
    OCRModel[MMOCRConfig, Union[str, ImageType]],
):
    def __init__(
        self,
        from_yaml: Optional[str] = None,
        configuration: Optional[MMOCRConfig] = None,
        string_replacement_map: Optional[Dict[str, str]] = None,
        init_for_inference: bool = False,
        is_multi_gpu_instance: bool = False,
        runtime: str = Runtime.DEFAULT,
    ) -> None:
        MMOCRModel.__init__(
            self,
            from_yaml=from_yaml,
            configuration=configuration,
            string_replacement_map=string_replacement_map,
            init_for_inference=init_for_inference,
            is_multi_gpu_instance=is_multi_gpu_instance,
            runtime=runtime,
            inferencer_type=TextRecInferencer,
        )
        OCRModel.__init__(
            self,
            configuration=self.configuration,
            init_for_inference=init_for_inference,
            runtime=runtime,
        )

    @staticmethod
    def __filter_result_score_by_mean(
        prediction: TextRecogDataSample, score_threshold: float
    ) -> Optional[float]:
        """
        Take the result of the mmocr prediction and determine the
        score of the word

        Args:
            prediction: The result of the inference
            score_threshold: The threshold for the score that has to be fulfilled

        Returns:
            The determined score
        """

        score: Optional[float] = None
        if len(prediction.pred_text.score) > 0:
            score = float(mean(prediction.pred_text.score))

            if score < score_threshold:
                score = None

        return score

    def _decode_mmocr_result(
        self, prediction: TextRecogDataSample
    ) -> List[OCRPerception]:
        ocr_texts: List[OCRPerception] = []

        score = self.__filter_result_score_by_mean(
            prediction=prediction,
            score_threshold=self.configuration.inference_config.score_threshold,
        )
        if score is not None:
            ocr_texts.append(
                OCRPerception(content=prediction.pred_text.item, score=score)
            )

        return ocr_texts

    @staticmethod
    def __generate_txt_annotations(
        annotation_handler: AnnotationHandler,
        output_file_path: str,
        data_root_dir: str,
        seperator: str,
        padding_ratio: float,
    ) -> None:

        annotations = annotation_handler.parse_training_annotations()

        annotation_writer = TextCropAnnotationWriter(
            output_file_path=output_file_path,
            data_root_dir=data_root_dir,
            seperator=seperator,
            rel_image_output_dir=os.path.dirname(output_file_path),
            padding_ratio=padding_ratio,
        )

        annotation_writer.write(annotations=annotations)

    def __update_training_data(self) -> None:

        dataset_config = self.configuration.train_config.dataset_config

        if dataset_config is None:
            return

        for context, config in zip(
            ("train", "test", "val"),
            (
                dataset_config.train_annotation_handler_config,
                dataset_config.test_annotation_handler_config,
                dataset_config.val_annotation_handler_config,
            ),
        ):
            if config is not None:
                self.__update_dataloader(
                    context=context,
                    annotation_handler_config=config,
                    padding_ratio=dataset_config.padding_ratio,
                )
        logger.info("Updated training data")

    def __update_dataloader(
        self,
        context: str,
        annotation_handler_config: AnnotationHandlerConfig,
        padding_ratio: float,
    ) -> None:
        context_dataloader = f"{context}_dataloader"
        logger.info(
            f"Update mmocr {context_dataloader} with annotations from "
            f"configuration.train_config.dataset_config.{context}_annotation_handler_config"
        )
        data_root_dir = self.get_training_output_dir()
        output_file_path = os.path.join(data_root_dir, f"{context}_annotations.txt")

        dataloader = self.cfg.get(context_dataloader, None)
        if not dataloader:
            return

        dataloader.dataset.ann_file = output_file_path
        dataloader.dataset.data_root = annotation_handler_config.to_dict()

        # necessary, otherwise the cropped image paths are not found
        dataloader.dataset["data_prefix"] = {"img_path": data_root_dir}

        MMOCRTextRecognitionModel.__generate_txt_annotations(
            annotation_handler=AnnotationHandler(
                configuration=annotation_handler_config
            ),
            output_file_path=output_file_path,
            data_root_dir=data_root_dir,
            seperator=dataloader.dataset.parser_cfg.separator,
            padding_ratio=padding_ratio,
        )

    def _train(
        self,
    ) -> None:

        self.__update_training_data()

        self._runner = MMOCRModel._build_runner(cfg=self.cfg)

        self._runner.train()


if __name__ == "__main__":
    MMDetectionModel.run(MMOCRTextRecognitionModel)
